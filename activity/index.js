/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

const getUserDetails = function () {
	const fullName = prompt('Enter full name');
	const age = prompt('Enter your age');
	const location = prompt('Enter your location');

	console.log(`Hello ${fullName}`);
	console.log(`You are ${age} years old`);
	console.log(`You are from ${location}`);
};

getUserDetails();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

const displayFavoriteArtists = function () {
	const favoriteArtists = [
		'Fall out boys',
		'The Script',
		'Eraserheads',
		'Childish Gambino',
		'Matchbox Twenty',
	];

	console.log(favoriteArtists);
};

displayFavoriteArtists();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

const printFavoriteMovies = function () {
	console.log('THE LORD OF THE RINGS: THE FELLOWSHIP OF THE RING: 91%');
	console.log('THE LORD OF THE RINGS: THE TWO TOWERS: 95%');
	console.log('THE LORD OF THE RINGS: THE RETURN OF THE KING: 93%');
	console.log('THE TRUMAN SHOW: 95%');
	console.log('Titanic: 87%');
};

printFavoriteMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

let printFriends = function printUsers() {
	alert('Hi! Please add the names of your friends.');
	let friend1 = prompt("Enter your first friend's name:");
	let friend2 = prompt("Enter your second friend's name:");
	let friend3 = prompt("Enter your third friend's name:");

	console.log('You are friends with:');
	console.log(friend1);
	console.log(friend2);
	console.log(friend3);
};

printFriends();
